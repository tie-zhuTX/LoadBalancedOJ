#pragma once

#include <iostream>
#include <string>

#include "util.hpp"

namespace ns_log
{
    using namespace ns_util;
    //定义日志等级
    enum{
        NORMAL,  //常规的
        DEBUG,   //调试日志
        WARNING, //警报，但是不影响使用
        ERROR,   //出错，用户的请求不可以继续了
        DEADLY   //系统整体挂了
    };
    std::ostream& Log(const std::string& level ,const std::string& file,int line)
    {
        //日志等级
        std::string ret="[";
        ret+=level;
        ret+="]";

        //添加时间
        ret+="[";
        ret+=TimeUtil::GetTime();
        ret+="]";

        //添加出错文件名称和出错行
        ret+="[";
        ret+=file;
        ret+=":";
        ret+=std::to_string(line);
        ret+="]";

        //不刷新，就会被暂存在缓冲区，返回之后还可以在上面拼接信息
        std::cout<<ret;

        return std::cout;
    }
    //使用方法：LOG(level)<<"information"<<endl;
    #define LOG(level) Log(#level,__FILE__,__LINE__)
}
