#pragma once 
#include <iostream>
#include <string>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/time.h>
#include <atomic>
#include <fstream>
#include <vector>
#include <boost/algorithm/string.hpp>

namespace ns_util
{
    //时间工具类
    class TimeUtil
    {
    public:
        static std::string GetTime()
        {
            //获取时间戳
            struct timeval tv;
            gettimeofday(&tv,nullptr);

            //localtime把时间戳转换为struct tm类型
            //调用strftime将tm类型转换成日期时间格式
            char datetime[64];
            strftime(datetime,sizeof(datetime),"%Y-%m-%d %H:%M:%S",localtime(&tv.tv_sec));
            return std::string(datetime);
        }
        //获得毫秒级别的时间戳
        static std::string GetMSTime()
        {
            struct timeval tv;
            gettimeofday(&tv,nullptr);
            //毫秒=秒*1000=微秒/1000
            return std::to_string(tv.tv_sec*1000+tv.tv_usec/1000);
        }
    };

    const std::string temp_path="./tempfile/";

    //用来拼接文件路径
    class PathUtil
    {
    public:
        static std::string AddSuffix(const std::string& file,const std::string suffix)
        {
            std::string ret=temp_path;
            ret+=file;
            ret+=suffix;
            return ret;
        }

        //****************** 编译使用的文件 *********************
        //构建完整的源文件名+路径
        //xxx ->./tempfile/xxx.cpp
        static std::string BuildSrc(const std::string& file)
        {
            return AddSuffix(file,".cpp");
        }
        //构建完整的可执行程序名+路径
        //xxx ->./tempfile/xxx.exe
        static std::string BuildExe(const std::string& file)
        {
            return AddSuffix(file,".exe");
        }
        //构建保存编译时报错的存储文件名+路径
        static std::string BuildCErr(const std::string& file)
        {
            return AddSuffix(file,".cerr");
        }

        //***************** 运行使用的零时文件 *********************
        static std::string BuildIn(const std::string& file)
        {
            return AddSuffix(file,".in");
        }
        static std::string BuildOut(const std::string& file)
        {
            return AddSuffix(file,".out");
        }
        //构建完整的保存运行时错误的文件名+路径
        static std::string BuildRErr(const std::string& file)
        {
            return AddSuffix(file,".rerr");
        }
    };
    //对文件的操作方法
    class FileUtil
    {
    public:
        //判断一个文件是否存在
        static bool IsExists(const std::string& file) 
        {
            //man 2 stat :获取文件属性
            //获取成功返回0，文件也一定存在。 -1失败
            struct stat st;//用来获取属性
            if(stat(file.c_str(),&st)==0)
            {
                return true;
            }
            return false;
        }
        //根据毫秒级时间戳和原子性递增的唯一值构建出一个可以保证唯一性的文件名
        static std::string GetUniqueName()
        {
            std::string ms=TimeUtil::GetMSTime();
            static std::atomic_uint key(0);
            key++;
            std::string key_s=std::to_string(key);
            return ms+key_s;
        }
        static bool WriteFile(const std::string& file,const std::string& code)
        {
            std::ofstream out(file);//写方式打开文件
            if(!out.is_open())
            {
                return false;
            }
            //写入
            out.write(code.c_str(),code.size());

            out.close();
            return true;
        }
        //用iskeep设定读取的时候是否保留\n
        static bool ReadFile(const std::string& file,std::string* mean,bool iskeep=false)
        {
            (*mean).clear();//先清理一下

            std::ifstream in(file);//读方式打开文件
            if(!in.is_open())
                return false;

            //构建读取内容
            std::string line;
            //getline不保存\n
            //getline重载和强制类型转换，所以可以写在while判断语句这里
            while(std::getline(in,line)){
                (*mean)+=line;
                (*mean)+=(iskeep ? "\n" : "");
            }

            in.close();
            return true;
        }
    };

    class StringUtil
    {
    public:
        //src:源字符串 ret:切分的结果都保存在vector sep:分隔符
        static void StringSplit(const std::string& src,std::vector<std::string> ret,std::string sep)
        {
            boost::split(ret,src,boost::is_any_of(sep),boost::algorithm::token_compress_on);
        }
    };
}