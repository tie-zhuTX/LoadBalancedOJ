#pragma once

#include <iostream>
#include <string>
#include <ctemplate/template.h>

//#include "oj_model_fd.hpp"
#include "oj_model_db.hpp"

namespace ns_view
{
    using namespace ns_model;

    const std::string html_path="./Rendered_html/";

    class View
    {
    public:
        View(){}
        ~View(){}
    public:
        //构建题目列表的网页
        /********************
         * 要显示的字段:
         * 题目编号 题目标题 题目难度
         * 使用表格显示
         * ******************/
        void ExpendListHtml(const std::vector<attribute>& all,std::string* dst_html)
        {
            //1.形成路径
            std::string src_html=html_path+"List.html";

            //2.形成数据字典
            ctemplate::TemplateDictionary d("list");
            //循环遍历式的添加每一行数据
            for(auto& att:all)
            {
                //向topic_list里面循环添加一个由d字典生成的叫做one的子字典
                ctemplate::TemplateDictionary* one=d.AddSectionDictionary("topic_list");
                one->SetValue("number",att.number);
                one->SetValue("title",att.title);
                one->SetValue("grade",att.grade);
            }

            //3.获取被渲染的网页
            ctemplate::Template* tpl=ctemplate::Template::GetTemplate(src_html,ctemplate::DO_NOT_STRIP);
        
            //4.完成渲染
            tpl->Expand(dst_html,&d);
        }
        //构建一道题的网页
        void ExpendOneHtml(const attribute& att,std::string* dst_html)
        {
            //1.形成路径
            std::string src_html=html_path+"One.html";

            //2.形成数据字典
            //需要的内容：编号，标题，难度，描述，代码编辑区
            ctemplate::TemplateDictionary d("One");
            d.SetValue("number",att.number);
            d.SetValue("title",att.title);
            d.SetValue("grade",att.grade);
            d.SetValue("desc",att.desc);
            d.SetValue("preset",att.preset);
            
            //3.获取被渲染的网页
            ctemplate::Template* tpl=ctemplate::Template::GetTemplate(src_html,ctemplate::DO_NOT_STRIP);

            //4.完成渲染
            tpl->Expand(dst_html,&d);

        }
    };
}