#pragma once
//MySQL数据库版本题库
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>
#include <fstream>
#include <string>

#include "../Comm/log.hpp"
#include "../Comm/util.hpp"
#include "include/mysql.h"

//把list文件中的题目信息加载到内存
//对外提供访问数据的接口

namespace ns_model
{
    using namespace ns_log;
    using namespace ns_util;
    //表示题目的属性信息
    struct attribute
    {
        std::string number;//题目编号
        std::string title;//题目标题
        std::string grade;//题目等级
        std::string desc;//题目描述
        //预设代码和测试用例拼接才能构成完整代码进行编译
        std::string preset;//给用户预设的代码
        std::string test;//测试用例
        int maxtime;//时间限制(s)
        int maxmem;//空间限制(KB)

    };

    const std::string table_name="oj_topics";
    const std::string host="127.0.0.1";
    const std::string user="oj_client";
    const std::string passwd="123456";
    const std::string db="oj";
    const int port=3306;

    class Model
    {
    public:
        Model()
        {}
        ~Model(){}
        //执行sql语句得到结果
        bool QueryMySQL(const std::string& sql,std::vector<attribute>* out)
        {
            //创建mysql句柄
            MYSQL* my=mysql_init(nullptr);

            //连接数据库
            if(mysql_real_connect(my,host.c_str(),user.c_str(),passwd.c_str(),db.c_str(),port,nullptr,0)==nullptr)
            {
                LOG(DEADLY)<<"连接数据库失败"<<std::endl;
                return false;
            }

            //设置连接编码，连接的默认编码是拉丁的，mysql是utf8的，不设置可能会出现乱码
            mysql_set_character_set(my,"utf8");

            LOG(NORMAL)<<"数据库连接成功"<<std::endl;

            //数据访问
            if(mysql_query(my,sql.c_str())!=0)
            {
                LOG(WARNING)<<sql<<"执行失败"<<std::endl;
                return false;
            }

            //提取结果
            MYSQL_RES* res=mysql_store_result(my);

            //获取结果
            int row=mysql_num_rows(res);//获得行数
            int col=mysql_num_fields(res);//获得列数

            for(int i=0;i<row;i++)
            {
                MYSQL_ROW line=mysql_fetch_row(res);//拿到一行记录
                //填充字段
                attribute att;
                att.number=line[0];
                att.title=line[1];
                att.grade=line[2];
                att.desc=line[3];
                att.preset=line[4];
                att.test=line[5];
                att.maxtime=atoi(line[6]);
                att.maxmem=atoi(line[7]);

                out->push_back(att);
            }


            //释放结果空间
            free(res);

            //关闭mysql连接
            mysql_close(my);
            return true;
        }
        //获取所有题目
        bool GetAllTopic(std::vector<attribute>* out)
        {
            std::string sql="select * from ";
            sql+=table_name;
            return QueryMySQL(sql,out);
        }
        //拿到指定的一个题目
        bool GetOneTopic(const std::string& number,attribute* out)
        {
            std::string sql="select * from ";
            sql+=table_name;
            sql+=" where number=";
            sql+=number;
            std::vector<attribute> ret;
            if(QueryMySQL(sql,&ret))
            {
                if(ret.size()==1){
                    *out=ret[0];
                    return true;
                }
            }
            return false;
        }
    };
}