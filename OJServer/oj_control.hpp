#pragma once

#include <iostream>
#include <string>
#include <pthread.h>
#include <assert.h>
#include <fstream>
#include <jsoncpp/json/json.h>
#include <algorithm>

//#include "oj_model_fd.hpp"
#include "oj_model_db.hpp"
#include "oj_view.hpp"
#include "../Comm/log.hpp"
#include "../Comm/util.hpp"
#include "../Comm/httplib.h"

namespace ns_control
{
    using namespace ns_log;
    using namespace ns_util;
    using namespace ns_model;
    using namespace ns_view;

    //提供服务的主机
    struct Machine
    {
    public:
        Machine()
            :ip("")
            ,port(0)
            ,count(0)
        {
            pthread_mutex_init(&mmtx,nullptr);
        }
        ~Machine(){
            pthread_mutex_destroy(&mmtx);
        }
    public:
        //增加负载数量
        void AddCount()
        {
            pthread_mutex_lock(&mmtx);
            count++;
            pthread_mutex_unlock(&mmtx);
        }
        //减少负载数量
        void DecCount()
        {
            pthread_mutex_lock(&mmtx);
            count--;
            pthread_mutex_unlock(&mmtx);
        }
        //离线的时候，要清空负载
        void CleanCount()
        {
            pthread_mutex_lock(&mmtx);
            count=0;
            pthread_mutex_unlock(&mmtx);
        }
        //获取负载
        uint64_t GetCount()
        {
            uint64_t ret=0;
            pthread_mutex_lock(&mmtx);
            ret=count;
            pthread_mutex_unlock(&mmtx);
            return ret;
        }
    public:
        std::string ip;//主机的IP
        int port;//主机的端口
        uint64_t count;//保存主机的负载情况
        pthread_mutex_t mmtx;//保护count
    };

    const std::string conf_path="./conf/machine.conf";

    //负载均衡模块
    class LoadBalance
    {
    public:
        LoadBalance()
        {
            pthread_mutex_init(&lmtx,nullptr);
            assert(LoadMachine(conf_path));
            LOG(NORMAL)<<"加载配置文件成功"<<std::endl;
        }
        ~LoadBalance(){
            pthread_mutex_destroy(&lmtx);
        }
    public:
        //加载配置文件，初始化machine结构
        bool LoadMachine(const std::string& machine_conf)
        {
            std::ifstream in(machine_conf);//打开主机的配置文件
            if(!in.is_open())
            {
                LOG(DEADLY)<<"获取主机配置文件失败"<<std::endl;
                return false;
            }
            
            //读取文件，获取每一个主机的IP+port
            std::string line;
            while(getline(in,line))
            {
                std::vector<std::string> cuted;
                StringUtil::StringSplit(line,&cuted,":");
                if(cuted.size()!=2)
                {
                    LOG(WARNING)<<"切分"<<line<<"失败"<<std::endl;
                    continue;
                }
                //填充machine字段
                Machine m;
                m.ip=cuted[0];
                m.port=atoi(cuted[1].c_str());
                m.count=0;
                //m.mtx=new ;
                online.push_back(machines.size());
                machines.push_back(m);
                
            }

            in.close();
            return true;
        }
        //负载均衡的选择主机请求服务
        //通过Machine的地址直接访问一个主机
        bool PickMachine(int* mid,Machine** m)
        {
            //1.获得一台主机，同时要更新主机的负载
            //2.离线一台主机
            pthread_mutex_lock(&lmtx);
            //负载均衡算法，
            //1.随机数+hash（随机挑主机）
            //2.轮询+hash（每次都找负载最小的）
            int online_count=online.size();//得到线性主机的数量
            if(online_count==0)
            {
                pthread_mutex_unlock(&lmtx);
                LOG(DEADLY)<<"所有主机都已经离线，后端没有编译服务"<<std::endl;
                return false;
            }
            //遍历找到负载最小的
            *mid=online[0];
            *m=&machines[online[0]];
            uint64_t min_count=machines[online[0]].count;//拿到一台主机的负载
            for(int i=0;i<online.size();i++)
            {
                if(min_count>machines[online[i]].count)
                {
                    min_count=machines[online[i]].count;//更新最小负载
                    *mid=online[i];//得到最小负载的机器id
                    *m=&machines[online[i]];//拿到最小负载的主机地址
                }
            }

            pthread_mutex_unlock(&lmtx);
            return true;
        }
        //请求失败下线机器,通过主机id离线主机
        bool OfflineMachine(int mid)
        {
            //把online的主机转移到offline
            pthread_mutex_lock(&lmtx);
            std::vector<int>::iterator it=online.begin();
            while(it!=online.end())
            {
                if(*it==mid)//找到了
                {
                    machines[*it].CleanCount();
                    offline.push_back(*it);//mid
                    online.erase(it);
                    break;
                }
                it++;
            }
            pthread_mutex_unlock(&lmtx);
        }
        //上线主机
        void OnlineMachine()
        {
            //策略，所有主机都离线时再统一上线
            pthread_mutex_lock(&lmtx);
            //从online的末尾开始，把offline的内容都插入到online
            online.insert(online.end(),offline.begin(),offline.end());
            offline.erase(offline.begin(),offline.end());
            pthread_mutex_unlock(&lmtx);

            LOG(NORMAL)<<"所有主机都已上线"<<std::endl;

        }
        //for test
        void ShowMachie()
        {
            pthread_mutex_lock(&lmtx);
            std::cout<<"当前在线的主机: ";
            for(int mid : online)
            {
                std::cout<<mid<<" ";
            }
            std::cout<<std::endl;
            std::cout<<"当前离线的主机: ";
            for(int mid : offline)
            {
                std::cout<<mid<<" ";
            }
            std::cout<<std::endl;
            pthread_mutex_unlock(&lmtx);
        }
    private:
        /*********************
         * 每台主机都有一个下标，用下标充当主机id
         * 主机的离线上线用一个不处理原本的数组
         * 而是用其他数组分别管理在线与下线的主机
         * ******************/
        std::vector<Machine> machines;//保存可以提供编译服务的所有主机
        std::vector<int> online;//管理在线的主机的id
        std::vector<int> offline;//管理离线主机的id
        //保证选择主机的线程安全
        pthread_mutex_t lmtx;//大锁
    };

    //业务逻辑控制器
    class Control
    {
    public:
        Control(){}
        ~Control(){}
    public:
        //上线主机
        void ROnline()
        {
            loadbalance.OnlineMachine();
        }
        //根据题目数据构建网页
        bool HtmlOfAll(std::string* html)
        {
            std::vector<attribute> all;
            if(model.GetAllTopic(&all))
            {
                //获取题目信息成功，

                //对题目列表排序
                sort(all.begin(),all.end(),[](const attribute& att1,const attribute& att2){
                    return atoi(att1.number.c_str())<atoi(att2.number.c_str());
                });

                //把所有的题目数据构建成html返回出去
                //LOG(DEBUG)<<"获取题目列表成功"<<std::endl;
                view.ExpendListHtml(all,html);
                //LOG(DEBUG)<<"题目列表渲染成功"<<std::endl;
            }
            else{
                *html="题目列表获取失败";
                return false;
            }
            return true;
        }
        //通过序号获得题目的网页
        bool HtmlOfOne(const std::string& number,std::string* html)
        {
            attribute att;
            if(model.GetOneTopic(number,&att))
            {
                //用number题目的att信息构建网页返回
                view.ExpendOneHtml(att,html);
            }
            else{
                *html="题目:"+number+"获取失败";
                return false;
            }
            return true;
        }
        //json_in:包含用户提交的所有信息
        //code input
        void Judge(const std::string& number,const std::string json_in,std::string* json_out)
        {
            //LOG(DEBUG)<<json_in<<"\number:"<<number<<std::endl;
            //根据题号获得题目的信息
            attribute att;
            model.GetOneTopic(number,&att);

            //1.对json_in反序列化，得到题目编号，代码，input数据
            Json::Reader reader;
            Json::Value in_root;
            reader.parse(json_in,in_root);
            std::string code=in_root["code"].asString();//得到用户提交的代码

            //2.拼接用户代码和预置代码，形成编译源码
            Json::Value compile_root;//用来形成编译代码json的value
            compile_root["input"]=in_root["input"].asString();
            compile_root["code"]=code+att.test;//用户代码+测试用例组成用于编译的源代码
            compile_root["maxtime"]=att.maxtime;
            compile_root["maxmem"]=att.maxmem;
            Json::StyledWriter writer;
            std::string compiler_json=writer.write(compile_root);//序列化，得到要发送给编译服务的json串

            //3.选择负载最低的主机（差错处理）
            //策略：要么全部挂掉，否则我就一直找，直到找到符合的主机
            while(true)
            {
                int mid=0;//得到被选中的机器id
                Machine* m=nullptr;//用来获得被选中主机的地址
                if(!loadbalance.PickMachine(&mid,&m))
                {
                    //表示所有主机都挂了
                    break;
                }
                
                
                //4.请求编译服务，得到结果
                httplib::Client clt(m->ip,m->port);
                m->AddCount();//访问之前要增加一下它的负载

                LOG(NORMAL)<<"选择主机: "<<mid<<":"<<m->ip<<":"<<m->port<<"成功. 负载是:"<<m->GetCount()<<std::endl;
                if(auto ret=clt.Post("/compiler_run",compiler_json,"application/json;charset=utf-8"))
                {
                    //请求得到了结果
                    if(ret->status==200)//200的状态码才表示请求完全成功
                    {
                        //5.把结果给json_out反给用户
                        *json_out=ret->body;
                        m->DecCount();//获得结果以后，减少负载
                        LOG(NORMAL)<<"编译和允许服务请求成功..."<<std::endl;
                        break;
                    }
                    //如果响应的状态码不是200，那就不会得到结果，会重新进入循环选择主机
                    m->DecCount();
                }
                else//没有得到响应，这个主机可能挂了
                {
                    LOG(ERROR)<<"请求的主机:"<<m->ip<<":"<<m->port<<"可能已经离线"<<std::endl;
                    loadbalance.OfflineMachine(mid);
                    //离线会把负载清零，所有不需要操作负载
                    loadbalance.ShowMachie();//for test
                }
            }

            

           
        }
    private:
        Model model;//提供数据
        View view;//提供网页渲染功能
        LoadBalance loadbalance;//提供负载均衡的选择主机的方法
    };
}