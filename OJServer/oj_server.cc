#include <iostream>
#include "../Comm/httplib.h"
#include "oj_control.hpp"


using namespace ns_control;

static Control* ctrl_ptr=nullptr;

void Recover(int signo)
{
    ctrl_ptr->ROnline();
}

int main()
{
    //主机恢复好之后，按ctrl \给服务发送该信号，然后调用方法重新上线
    signal(SIGQUIT,Recover);

    //服务路由功能
    httplib::Server svr; 
    Control ctrl;//方便上线主机
    ctrl_ptr=&ctrl;

    svr.set_base_dir("./wwwroot");

    //获取题目
    svr.Get("/topic_list",[&ctrl](const httplib::Request& req,httplib::Response& resp){
       
        //返回一个包含题目的html网页
        std::string html;
        ctrl.HtmlOfAll(&html);
        resp.set_content(html,"text/html;charset=utf-8");

    });

    //根据题目编号，获取题目
    //(\d+):正则表达式，\d表示匹配数字，+表示匹配一个或多个 R"()":保持字符串的原貌
    svr.Get(R"(/topic/(\d+))",[&ctrl](const httplib::Request& req,httplib::Response& resp){
        std::string number=req.matches[1];//拿到正则匹配到的内容
        std::string html;
        ctrl.HtmlOfOne(number,&html);
        resp.set_content(html,"text/html;charset=utf-8");
    });

    //用户提交，我们需要判题
    svr.Post(R"(/judge/(\d+))",[&ctrl](const httplib::Request& req,httplib::Response& resp){
        std::string number=req.matches[1];//拿到正则匹配到的内容
        std::string json_out;
        ctrl.Judge(number,req.body,&json_out);
        resp.set_content(json_out,"application/json;charset=utf-8");


        //resp.set_content("判断的题目:"+number,"text/plain;charset=utf-8");
    });


    svr.listen("0.0.0.0",8848);
    return 0;
}

