
#ifndef COMPILER_ONLINE
#include "preset.cpp"
#endif

void Test1()
{
    //定义临时对象进行调用
    bool ret=Solution().isParlindrome(1234321);
    if(ret)
        std::cout<<"测试用例1通过!"<<std::endl;
    else
        std::cout<<"测试用例1没有通过,用例值: 1234321"<<std::endl;

}

void Test2()
{
    //定义临时对象进行调用
    bool ret=Solution().isParlindrome(-10);
    if(!ret)
        std::cout<<"测试用例2通过!"<<std::endl;
    else
        std::cout<<"测试用例2没有通过,用例值: -10"<<std::endl;

}

int main()
{
    Test1();
    Test2();
    return 0;
}