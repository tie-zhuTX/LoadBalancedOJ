#pragma once
//文件版本题库
#include <iostream>
#include <string>
#include <unordered_map>
#include <assert.h>
#include <vector>
#include <fstream>
#include <string>

#include "../Comm/log.hpp"
#include "../Comm/util.hpp"

//把list文件中的题目信息加载到内存
//对外提供访问数据的接口

namespace ns_model
{
    using namespace ns_log;
    using namespace ns_util;
    //表示题目的属性信息
    struct attribute
    {
        std::string number;//题目编号
        std::string title;//题目标题
        std::string grade;//题目等级
        std::string desc;//题目描述
        int maxtime;//时间限制(s)
        int maxmem;//空间限制(KB)
        //预设代码和测试用例拼接才能构成完整代码进行编译
        std::string preset;//给用户预设的代码
        std::string test;//测试用例
    };

    const std::string list_path="./TopicBank/topic_list.txt";//列表的路径
    const std::string bank_path="./TopicBank/";//题库路径
    class Model
    {
    public:
        Model()
        {
            assert(LoadTopicList(list_path));
        }
        ~Model(){}
        //把列表的题目加载到内存
        bool LoadTopicList(const std::string& list_path)
        {
            //加载TopicBack/topic.list+题目编号对应的

            //按行配置，所以按行读取
            std::ifstream in(list_path);
            if(!in.is_open()){
                LOG(DEADLY)<<"{加载题库失败，题库文件不存在}"<<std::endl;
                return false;
            }
            //按行读取
            std::string line;
            while(std::getline(in,line))
            {
                //行格式：编号 标题 等级 时间限制 空间限制
                //需要字符串切分工具，把每一部分单独切出来
                std::vector<std::string> cuted;
                std::string sep=" ";
                StringUtil::StringSplit(line,&cuted,sep);
                if(cuted.size()!=5)
                {
                    //这一行配置有问题，只能不要了
                    LOG(WARNING)<<"{部分题目加载失败，请检查文件格式}"<<std::endl;
                    continue;
                }

                //填充属性
                attribute att;
                att.number=cuted[0];
                att.title=cuted[1];
                att.grade=cuted[2];
                att.maxtime=atoi(cuted[3].c_str());
                att.maxmem=atoi(cuted[4].c_str());

                //拿到每道题目对应的路径
                std::string topic_path=bank_path+att.number+"/";

                FileUtil::ReadFile(topic_path+"desc.txt",&(att.desc),true);
                FileUtil::ReadFile(topic_path+"preset.cpp",&(att.preset),true);
                FileUtil::ReadFile(topic_path+"test.cpp",&(att.test),true);

                topic_map[att.number]=att;
                LOG(NORMAL)<<"{添加题目:" <<att.number<<"成功}"<<std::endl;
            }
            LOG(NORMAL)<<"{题库加载...成功}"<<std::endl;
            in.close();
            return true;
        }
        //获取所有题目
        bool GetAllTopic(std::vector<attribute>* out)
        {
            if(topic_map.empty())
            {
                LOG(ERROR)<<"{用户获取题库失败}"<<std::endl;
                return false;
            }
            for(auto& e :topic_map)
            {
                out->push_back(e.second);
                LOG(NORMAL)<<"获取到题目:"<<e.second.number<<std::endl;
            }
            return true;
        }
        //拿到指定的一个题目
        bool GetOneTopic(const std::string& number,attribute* out)
        {
            std::unordered_map<std::string,attribute>::iterator it=topic_map.find(number);
            if(it==topic_map.end()){
                LOG(ERROR)<<"{获取题目: "<<number<<"失败}"<<std::endl;
                return false;
            }
            *out=it->second;
        }

    private:
        //题目编号->题目属性
        std::unordered_map<std::string,attribute> topic_map;
    };
}