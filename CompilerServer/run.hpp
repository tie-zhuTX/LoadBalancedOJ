#pragma once

#include <iostream>
#include <string>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <sys/resource.h>


#include "../Comm/util.hpp"
#include "../Comm/log.hpp"

namespace ns_run
{
    using namespace ns_util;
    using namespace ns_log;
    class Run
    {
    public:
        Run(){}
        ~Run(){}
    public:
        static void SetLimit(int time,int mem)
        {
            //设置时间
            struct rlimit timert;
            timert.rlim_max=RLIM_INFINITY;
            timert.rlim_cur=time;
            setrlimit(RLIMIT_CPU,&timert);
            //设置内存
            struct rlimit memrt;
            memrt.rlim_max=RLIM_INFINITY;
            memrt.rlim_cur=mem*1024;//因为设计的单位是KB
            setrlimit(RLIMIT_AS,&memrt);
        }
        //如果运行异常，返回异常的信号
        //运行正常，就返回0
        //模块本身出现错误，返回小于0
        //maxtime:最多允许运行多久  maxmem:最多可以使用多少空间但是是KB
        static int Running(const std::string& file,int maxtime,int maxmem)
        {
            /********************************
             * 1.代码跑完，结果正确
             * 2.代码跑完，结果错误
             * 3.运行中崩溃
             * 运行模块不需要判定结果是否正确，是需要判断是否跑完
             * 标准输入:定向到.in文件，暂不处理
             * 标准输出:定向到.out文件，用来判断是否正确
             * 标准错误:定向到.err文件，运行时错误的信息
            *********************************/
            //获得对应的带路径的完整文件名
            std::string exefile=PathUtil::BuildExe(file);
            std::string infile =PathUtil::BuildIn(file);
            std::string outfile=PathUtil::BuildOut(file);
            std::string errfile=PathUtil::BuildRErr(file);
            //打开对应的文件
            umask(0);
            int in_fd=open(infile.c_str(),O_CREAT|O_RDONLY,0644);
            int out_fd=open(outfile.c_str(),O_CREAT|O_WRONLY,0644);
            int rerr_fd=open(errfile.c_str(),O_CREAT|O_WRONLY,0644);
            pid_t id=fork();
            if(in_fd<0||out_fd<0||rerr_fd<0)
            {
                LOG(ERROR)<<"{打开运行时标准文件失败}"<<std::endl;
                return -1;//打开文件失败
            }
            if(id<0)
            {
                LOG(ERROR)<<"{创建运行子进程失败}"<<std::endl;
                close(in_fd);
                close(out_fd);
                close(rerr_fd);
                return -2;//创建子进程失败
            }
            else if(id==0)//子进程
            {
                //重定向到三个文件
                dup2(in_fd,0);
                dup2(out_fd,1);
                dup2(rerr_fd,2);

                //添加限制
                SetLimit(maxtime,maxmem);
                
                //我们自己的程序环境变量没有，并且还不在当前路径下，所有参数必须带路径
                execl(exefile.c_str(),exefile.c_str(),nullptr);

                exit(1);
            }
            else//父进程
            {
                //父进程不关心这些文件
                close(in_fd);
                close(out_fd);
                close(rerr_fd);

                int status =0;//用来获得退出时是否异常。判断是否崩溃
                waitpid(id,&status,0);
                LOG(NORMAL)<<"{运行完成，获取状态码:"<< (status&0x7F) << "}"<<std::endl;
                return status&0x7F;
            }
        }
    };
}