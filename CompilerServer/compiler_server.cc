#include "compiler_run.hpp"
#include "../Comm/httplib.h"

using namespace ns_compiler_run;
void Usage(char* proc)
{
    std::cerr<<"Usage: "<<"\n\t"<<proc<<"port"<<std::endl;
}
int main(int argc,char* argv[])
{
    if(argc!=2){
        Usage(argv[0]);
        return 1;
    }
    httplib::Server svr;

    svr.Post("/compiler_run",[](const httplib::Request& req,httplib::Response& resp){
        std::string json_in=req.body;//拿到用户串来的json串
        std::string json_out;//存放编译运行服务后的返回json串
        if(!json_in.empty())
        {
            CompilerRun::Start(json_in,&json_out);

            //给用户构建响应
            resp.set_content(json_out,"application/json;charset=utf-8");
        }

    });

    svr.listen("0.0.0.0",atoi(argv[1]));
}


    // //************** 测试compiler_run模块 *****************
    // std::string json_in;
    // Json::Value rootin;
    // rootin["code"]=R"(#include<iostream>
    // int main(){
    //     std::cout<<"测试编译运行"<<std::endl;
    //     int* p=new int[1024*1024*20];
    //     return 0;
    //     })";
    // rootin["input"]="";
    // rootin["maxtime"]=1;
    // rootin["maxmem"]=1024*30;//30M

    // Json::FastWriter writer;
    // json_in=writer.write(rootin);
    // std::cout<<json_in.c_str()<<std::endl;

    // std::string json_out;//给用户返回的
    // CompilerRun::Start(json_in,&json_out);
    // std::cout<<json_out.c_str()<<std::endl;
    // return 0;
    //*********************************************************
