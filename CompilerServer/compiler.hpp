#pragma once

#include <iostream>
#include <unistd.h>
#include <sys/wait.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "../Comm/util.hpp"
#include "../Comm/log.hpp"

namespace ns_compiler
{
    using namespace ns_util;
    using namespace ns_log;

    class Compiler
    {
    public:
        Compiler(){}
        ~Compiler(){}
        //codefile:要编译的文件名 编译成功返回true 失败返回false
        //在当前目录下会有一个tempfile保存各种临时文件
        //有了文件名，至少会创建三个文件：
        //1.根据文件名构建源文件         
        //2.编译成功形成的可执行程序      
        //3.编译失败用来保存结果的临时文件 
        static bool Compile(const std::string& file)
        {
            pid_t id=fork();
            if(id<0){//创建子进程失败
                LOG(ERROR)<<"{创建子进程失败}"<<std::endl;
                return false;
            }
            else if(id==0)//**************  子进程  ***********************
            {
                //先打开错误文件，方便编译出错存储信息
                //以写入的方式创建
                umask(0);//请零umask，让权限不会被umask干扰
                int errfd=open(PathUtil::BuildCErr(file).c_str(),O_CREAT|O_WRONLY,0644);
                if(errfd<0)
                {
                    //打开文件失败，无法给用户返回信息，直接退出吧
                    LOG(WARNING)<<"{形成.cerr文件失败}"<<std::endl;
                    exit(1);
                }
                //重定向，当替换程序执行时打印到标准错误的内容定向到我们的文件
                //newfd作为oldfd的拷贝
                dup2(errfd,2);

                //调用程序替换函数执行代码
                //g++ -o dst src -std=c++11
                execlp("g++","g++","-o",PathUtil::BuildExe(file).c_str(),\
                PathUtil::BuildSrc(file).c_str(),"-D","COMPILER_ONLINE","-std=c++11",nullptr);

                //如果替换失败，直接退出
                LOG(ERROR)<<"{程序替换失败，没有编译}"<<std::endl;
                exit(2);
            }
            else//*********************  父进程  ******************************
            {
                waitpid(id,nullptr,0);
                //子进程退出，判断编译是否成功,看有没有形成可执行程序
                if(FileUtil::IsExists(PathUtil::BuildExe(file)))
                {
                    LOG(NORMAL)<<"{"<<PathUtil::BuildSrc(file)<<"文件编译成功}"<<std::endl;
                    return true;
                }
            }
            LOG(ERROR)<<"{编译失败，没有形成可执行程序}"<<std::endl;
            return false;
        }
    private:
    };
}