#pragma once

#include <jsoncpp/json/json.h>
#include <signal.h>
#include <unistd.h>

#include "compiler.hpp"
#include "run.hpp"
#include "../Comm/util.hpp"
#include "../Comm/log.hpp"

namespace ns_compiler_run
{
    using namespace ns_log;
    using namespace ns_util;
    using namespace ns_compiler;
    using namespace ns_run;

    class CompilerRun
    {
    public:
        CompilerRun(){}
        ~CompilerRun(){}
    public:
    //用来获得状态码的含义
    static std::string MeanOfStat(int stat,const std::string& filename)
    {
        std::string mean;
        switch (stat)
        {
        case 0:
            mean="编译运行成功";
            break;
        case -1:
            mean="提交代码为空";
            break;
        case -2:
            mean="发生了未知错误";
            break;
        case -3://拿到编译错误
            FileUtil::ReadFile(PathUtil::BuildCErr(filename),&mean,true);
            break;
        case SIGABRT://6
            mean="内存超过限制";
            break;
        case SIGFPE://8
            mean="除零错误";
            break;
        case SIGXCPU://24
            mean="运行超时";
            break;
        default:
            mean="其他错误: "+std::to_string(stat);
            break;
        }
        return mean;
    }
    static void RemoveTempFile(const std::string& file)
    {
        //形成的临时文件个是是不确定的，但是最多只有我们定义的6个
        //判断是否存在，存在就移除，不存在就删除
        std::string src=PathUtil::BuildSrc(file);
        if(FileUtil::IsExists(src))
            unlink(src.c_str());

        std::string exe=PathUtil::BuildExe(file);
        if(FileUtil::IsExists(exe))
            unlink(exe.c_str());

        std::string cerr=PathUtil::BuildCErr(file);
        if(FileUtil::IsExists(cerr))
            unlink(cerr.c_str());
        
        std::string in=PathUtil::BuildIn(file);
        if(FileUtil::IsExists(in))
            unlink(in.c_str());

        std::string out=PathUtil::BuildOut(file);
        if(FileUtil::IsExists(out))
            unlink(out.c_str());

        std::string rerr=PathUtil::BuildRErr(file);
        if(FileUtil::IsExists(rerr))
            unlink(rerr.c_str());
    } 
        /********************************
        * json_in:输入的内容
        * code:用户提交的代码
        * input:用户的自测输入
        * maxtime:最大时间
        * maxmem:最大空间
        * 
        * json_out:
        * 必填
        * stat:设定的状态码
        * mean:状态码对应的含义
        * 选填
        * 运行完的结果
        * 运行完的报错
        **********************************/
        static void Start(const std::string& json_in,std::string* json_out)
        {
            Json::Value rootin;
            Json::Reader rd;
            rd.parse(json_in,rootin);
            std::string code=rootin["code"].asString();
            std::string input=rootin["input"].asString();
            int maxtime=rootin["maxtime"].asInt();
            int maxmem=rootin["maxmem"].asInt();

            //用来给用户返回的json串
            Json::Value rootout;

            //用来设置状态码
            int stat;
            //运行模块的返回值
            int sig;
            //存放具有唯一性的文件名
            std::string filename;

            if(code.size()==0)
            {
                //没有提交代码
                stat=-1;
                goto END;
            }

            //获得一个具有唯一性的文件名
            filename=FileUtil::GetUniqueName();
            //把代码写到文件里,形成临时源文件
            if(!FileUtil::WriteFile(PathUtil::BuildSrc(filename),code))
            {
                //写入文件失败
                stat=-2;
                goto END;
            }

            if(!Compiler::Compile(filename))
            {
                //编译失败
                stat=-3;
                goto END;
            }

            sig=Run::Running(filename,maxtime,maxmem);
            if(sig<0)//内部错误
            {
                stat=2;
            }
            else
            {
                //运行时出错，代码崩了
                //运行成功返回0，都符合我们状态码的定义
                stat=sig;
            }
        END:
            //********** 在这里统一处理 *************
            rootout["stat"]=stat;
            rootout["mean"]=MeanOfStat(stat,filename);
            if(stat==0)//运行成功
            {
                std::string out;
                FileUtil::ReadFile(PathUtil::BuildOut(filename),&out,true);
                rootout["out"]=out;
                
                std::string rerr;
                FileUtil::ReadFile(PathUtil::BuildRErr(filename),&rerr,true);
                rootout["rerr"]=rerr;
            }

            //序列化
            Json::StyledWriter writer;
            *json_out=writer.write(rootout);

            //清理掉相关的临时文件
            RemoveTempFile(filename);
        }
    };
}

