# 编译功能
.PHONY:all
all:
	@cd CompilerServer;\
	make;\
	cd -;\
	cd OJServer;\
	make;\
	cd -;

# 部署功能
.PHONY:submit
submit:
	@mkdir -p output/CompilerServer;\
	mkdir -p output/OJServer;\
	cp -rf CompilerServer/compiler_server output/CompilerServer;\
	cp -rf CompilerServer/tempfile output/CompilerServer;\
	cp -rf OJServer/oj_server output/OJServer;\
	cp -rf OJServer/conf output/OJServer;\
	cp -rf OJServer/lib output/OJServer;\
	cp -rf OJServer/TopicBank output/OJServer;\
	cp -rf OJServer/Rendered_html output/OJServer;\
	cp -rf OJServer/wwwroot output/OJServer;\

# 清除功能
.PHONY:clean
clean:
	@cd CompilerServer;\
	make clean;\
	cd -;\
	cd OJServer;\
	make clean;\
	cd -;\
	rm -rf output;